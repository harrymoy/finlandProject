/* jshint esnext:true */
var restify = require('restify'),
	logic = require('./logic.js'),
	storage = require('node-persist');

var server = restify.createServer();

server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.authorizationParser());

server.post('/data', function(req, res) {
	var payload = req.body;
	console.log(payload);
	var x = logic.evaluateProgram(payload.rules, payload.facts);
	res.setHeader('Content-Type', 'application/json');
	res.send(x);
});

server.get('/recent', function(req, res) {
	var rules = logic.retrieveRules();
	var facts = logic.retrieveFacts();
	var data = logic.evaluateProgram(rules, facts);
	res.setHeader('Content-Type', 'application/json');
	res.send(data);
});

server.get('/', function(req, res) {
	var item = logic.sampleExpression();
	res.setHeader('Content-Type', 'application/json');
	res.send(item)
});

var port = process.env.PORT || 8080;
server.listen(port, function(err) {
	if (err) {
		console.log(err);
	} else {
		console.log("Module is ready at port " + port);
	}
});