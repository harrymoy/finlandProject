import os
class imageGetter(object):
	def getMostRecentImage(self):
			logdir='.' # path to your log directory
			logfiles = sorted([ f for f in os.listdir(logdir) if f.startswith('IMG_')])
			recentImage = logfiles[-1]
			return recentImage 
