var esprima = require('esprima'),
	evaluate = require('static-eval'),
	readline = require('readline-sync');


function mainProg() {
	var rule = readline.question("Enter the rule: ");
	var fact = readline.question("Enter the facts: ");
	fact = stripFacts(fact);
	evaluateRule(rule, fact);
}

function evaluateRule(rule, fact) {
	var ruleAST = esprima.parse(rule);
	console.log(evaluate(ruleAST.body[0].expression, fact));
}

function stripFacts(fact) {
	console.log(fact);
    return fact.slice(1, -1).split(',').map(function(el) {
        return el.trim();
    }).map(function(el) {
        return el.split('=');
    }).reduce(function(result, pair) {
        result[pair[0].trim()] = Number(pair[1].trim());
        return result;
    }, {});
}

mainProg();