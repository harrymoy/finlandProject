import serial
from pymongo import MongoClient

ser = serial.Serial('/dev/cu.usbserial-14P00776', 9600)

client = MongoClient()
db = client.prog

try:
	while True:
		x = ser.readline()
		x = bool(x)
		if x == True:
			print "received"
			result = db.progs.insert_one({"rules":"x * 3 > y * 4", "facts": "{x=7, y = 5"})

except Exception, e:
	ser.close()