/* jshint esnext:true */
var esprima = require('esprima'),
	evaluate = require('static-eval'),
	storage = require('node-persist'),
	readline = require('readline-sync');

exports.evaluateProgram = function(rules, facts) {
	const result = evaluateRule(rules, stripFacts(facts));
	console.log(result);
	return result;
};

function initStorage() {
	storage.init({
    	dir:'relative/path/to/persist',
    	stringify: JSON.stringify,
    	parse: JSON.parse,
    	encoding: 'utf8',
    	logging: false,  // can also be custom logging function
    	continuous: true,
    	interval: false,
    	ttl: false // ttl* [NEW], can be true for 24h default or a number in MILLISECONDS
	});
}

exports.sampleExpression = function() {
	var facts = "{x=3, y=4}";
	var rules = "x * 10 > y * 9";
	var result = evaluateRule(rules, stripFacts(facts));
	return result;
};

exports.saveFacts = function(facts) {
	initStorage();
	storage.setItem('facts', facts);
	// storage.persistKeySync();
};

exports.saveRules = function(rules) {
	initStorage();
	storage.setItem('rules', rules);
	// storage.persistKeySync();
};

exports.retrieveFacts= function() {
	initStorage();
	var item = storage.getItem('facts');
	return item;
};

exports.retrieveRules = function() {
	initStorage();
	var item = storage.getItem('rules');
	return item;
};

function evaluateRule(rule, fact) {
	var ruleAST = esprima.parse(rule);
	console.log(ruleAST.body);
	return evaluate(ruleAST.body[0].expression, fact);
}

function stripFacts(fact) {
	console.log(fact);
    return fact.slice(1, -1).split(',').map(function(el) {
        return el.trim();
    }).map(function(el) {
        return el.split('=');
    }).reduce(function(result, pair) {
        result[pair[0].trim()] = Number(pair[1].trim());
        return result;
    }, {});
}
