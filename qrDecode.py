import zbarlight
from PIL import Image
import os, os.path

path = "./"

class qrDecoder(object):
	def decodeQr(self, imageToDecode):
		codes = zbarlight.qr_code_scanner('qrcode.enable', 2000, 2000, imageToDecode)
		return codes

qrDec = qrDecoder()
for file in os.listdir(path):
	print file
	if file == "IMG.png":
		code = qrDec.decodeQr(file)
		print code
	else:
		print "Nada"
